import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CategoriesModule } from './categories/categories.module';
import { ArticlesModule } from './articles/articles.module';
import { UsersModule } from './users/users.module';

@Module({
	imports: [CategoriesModule, ArticlesModule, UsersModule],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
