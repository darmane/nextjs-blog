import { hashSync, compareSync } from 'bcryptjs';

export const hash = (value: string): string => {
	return hashSync(value, 12);
};

export const compare = (value: string, hash: string): boolean => {
	return compareSync(value, hash);
};
