import {
	Controller,
	Get,
	Post,
	Body,
	Put,
	Param,
	Delete,
	NotFoundException,
	ConflictException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from '@prisma/client';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { hash } from 'src/lib/hash';

@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Post()
	async create(@Body() user: CreateUserDto): Promise<User> {
		const foundUser = await this.usersService.findOne({
			email: user.email,
		});

		if (foundUser) {
			throw new ConflictException();
		}

		user.password = hash(user.password);

		const createdUser = await this.usersService.create(user);
		createdUser?.password && delete createdUser.password;
		return createdUser;
	}

	@Get()
	findAll(): Promise<User[]> {
		return this.usersService.findAll({});
	}

	@Get(':id')
	async findOne(@Param('id') id: string): Promise<User> {
		const foundUser = await this.usersService.findOne({
			id: id,
		});

		if (!foundUser) {
			throw new NotFoundException();
		}

		return foundUser;
	}

	@Put(':id')
	async update(@Param('id') id: string, @Body() user: UpdateUserDto) {
		const foundUser = await this.usersService.findOne({
			id: id,
		});

		if (!foundUser) {
			throw new NotFoundException();
		}

		user.password = hash(user.password);

		const updatedUser = await this.usersService.update({
			where: { id: id },
			data: user,
		});
		updatedUser?.password && delete updatedUser.password;
		return updatedUser;
	}

	@Delete(':id')
	async remove(@Param('id') id: string) {
		const foundUser = await this.usersService.findOne({
			id: id,
		});

		if (!foundUser) {
			throw new NotFoundException();
		}

		return this.usersService.remove({ id: id });
	}
}
