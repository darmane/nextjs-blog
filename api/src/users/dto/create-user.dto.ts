import { Role, User } from '@prisma/client';
import {
	IsBoolean,
	IsEmail,
	IsIn,
	IsNotEmpty,
	IsOptional,
	IsString,
	Matches,
} from 'class-validator';

export class CreateUserDto implements User {
	id: string;

	@IsString()
	@IsNotEmpty()
	name: string;

	@IsEmail()
	email: string;

	@IsString()
	@IsNotEmpty()
	@Matches(
		/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
	)
	password: string;

	@IsOptional()
	@IsString()
	@IsNotEmpty()
	image: string;

	@IsOptional()
	@IsIn(['ADMIN', 'AUTHOR'])
	role: Role;

	@IsOptional()
	@IsBoolean()
	isBlocked: boolean;
}
