import { Category } from '@prisma/client';
import { IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator';

export class CreateCategoryDto implements Category {
	id: string;

	@IsString()
	@IsNotEmpty()
	name: string;

	@IsString()
	@IsNotEmpty()
	@Matches(/^[a-z0-9]+(?:(?:-|_)+[a-z0-9]+)*$/)
	slug: string;

	@IsOptional()
	@IsString()
	@IsNotEmpty()
	image: string;
}
