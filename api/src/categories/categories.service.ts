import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Category, Prisma } from '@prisma/client';

@Injectable()
export class CategoriesService {
	constructor(private prisma: PrismaService) {}

	async create(data: Prisma.CategoryCreateInput): Promise<Category> {
		return this.prisma.category.create({
			data,
		});
	}

	async findAll(params: {
		skip?: number;
		take?: number;
		cursor?: Prisma.CategoryWhereUniqueInput;
		where?: Prisma.CategoryWhereInput;
		orderBy?: Prisma.CategoryOrderByWithRelationInput;
	}): Promise<Category[]> {
		const { skip, take, cursor, where, orderBy } = params;
		return this.prisma.category.findMany({
			skip,
			take,
			cursor,
			where,
			orderBy,
		});
	}

	async findOne(
		userWhereUniqueInput: Prisma.CategoryWhereUniqueInput
	): Promise<Category | null> {
		return this.prisma.category.findUnique({
			where: userWhereUniqueInput,
		});
	}

	async update(params: {
		where: Prisma.CategoryWhereUniqueInput;
		data: Prisma.CategoryUpdateInput;
	}): Promise<Category> {
		const { where, data } = params;
		return this.prisma.category.update({
			data,
			where,
		});
	}

	async remove(where: Prisma.CategoryWhereUniqueInput): Promise<Category> {
		return this.prisma.category.delete({
			where,
		});
	}
}
