import {
	Controller,
	Get,
	Post,
	Body,
	Param,
	Delete,
	Put,
	NotFoundException,
	ConflictException,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { Category } from '@prisma/client';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Controller('categories')
export class CategoriesController {
	constructor(private readonly categoriesService: CategoriesService) {}

	@Post()
	async create(@Body() category: CreateCategoryDto): Promise<Category> {
		const foundCategory = await this.categoriesService.findOne({
			slug: category.slug,
		});

		if (foundCategory) {
			throw new ConflictException();
		}

		return this.categoriesService.create(category);
	}

	@Get()
	findAll(): Promise<Category[]> {
		return this.categoriesService.findAll({});
	}

	@Get(':slug')
	async findOne(@Param('slug') slug: string): Promise<Category> {
		const foundCategory = await this.categoriesService.findOne({
			slug: slug,
		});

		if (!foundCategory) {
			throw new NotFoundException();
		}

		return foundCategory;
	}

	@Put(':id')
	async update(@Param('id') id: string, @Body() category: UpdateCategoryDto) {
		const foundCategory = await this.categoriesService.findOne({
			id: id,
		});

		if (!foundCategory) {
			throw new NotFoundException();
		}

		return this.categoriesService.update({
			where: { id: id },
			data: category,
		});
	}

	@Delete(':id')
	async remove(@Param('id') id: string) {
		const foundCategory = await this.categoriesService.findOne({
			id: id,
		});

		if (!foundCategory) {
			throw new NotFoundException();
		}

		return this.categoriesService.remove({ id: id });
	}
}
