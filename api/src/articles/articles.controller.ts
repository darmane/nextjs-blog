import {
	Controller,
	Get,
	Post,
	Body,
	Put,
	Param,
	Delete,
	NotFoundException,
} from '@nestjs/common';
import { Article } from '@prisma/client';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';

@Controller('articles')
export class ArticlesController {
	constructor(private readonly articlesService: ArticlesService) {}

	@Post()
	create(@Body() article: CreateArticleDto): Promise<Article> {
		return this.articlesService.create({
			...article,
			author: { connect: { id: article.authorId } },
			category: { connect: { id: article.categoryId } },
		});
	}

	@Get()
	findAll(): Promise<Article[]> {
		return this.articlesService.findAll({});
	}

	@Get(':slug')
	async findOne(@Param('slug') slug: string): Promise<Article> {
		const foundArticle = await this.articlesService.findOne({
			slug: slug,
		});

		if (!foundArticle) {
			throw new NotFoundException();
		}

		return foundArticle;
	}

	@Put(':id')
	async update(@Param('id') id: string, @Body() article: UpdateArticleDto) {
		const foundArticle = await this.articlesService.findOne({
			id: id,
		});

		if (!foundArticle) {
			throw new NotFoundException();
		}

		return this.articlesService.update({
			where: { id: id },
			data: {
				...article,
				author: {
					connect: { id: article.authorId ?? foundArticle.authorId },
				},
				category: {
					connect: {
						id: article.categoryId ?? foundArticle.categoryId,
					},
				},
			},
		});
	}

	@Delete(':id')
	async remove(@Param('id') id: string) {
		const foundArticle = await this.articlesService.findOne({
			id: id,
		});

		if (!foundArticle) {
			throw new NotFoundException();
		}

		return this.articlesService.remove({ id: id });
	}
}
