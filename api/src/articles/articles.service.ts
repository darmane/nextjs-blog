import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { Article, Prisma } from '@prisma/client';

@Injectable()
export class ArticlesService {
	constructor(private prisma: PrismaService) {}

	async create(data: Prisma.ArticleCreateInput): Promise<Article> {
		return this.prisma.article.create({
			data,
		});
	}

	async findAll(params: {
		skip?: number;
		take?: number;
		cursor?: Prisma.ArticleWhereUniqueInput;
		where?: Prisma.ArticleWhereInput;
		orderBy?: Prisma.ArticleOrderByWithRelationInput;
	}): Promise<Article[]> {
		const { skip, take, cursor, where, orderBy } = params;
		return this.prisma.article.findMany({
			skip,
			take,
			cursor,
			where,
			orderBy,
		});
	}

	async findOne(
		userWhereUniqueInput: Prisma.ArticleWhereUniqueInput
	): Promise<Article | null> {
		return this.prisma.article.findUnique({
			where: userWhereUniqueInput,
		});
	}

	async update(params: {
		where: Prisma.ArticleWhereUniqueInput;
		data: Prisma.ArticleUpdateInput;
	}): Promise<Article> {
		const { where, data } = params;
		return this.prisma.article.update({
			data,
			where,
		});
	}

	async remove(where: Prisma.ArticleWhereUniqueInput): Promise<Article> {
		return this.prisma.article.delete({
			where,
		});
	}
}
