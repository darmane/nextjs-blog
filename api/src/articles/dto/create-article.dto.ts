import { Article } from '@prisma/client';
import {
	IsDateString,
	IsNotEmpty,
	IsOptional,
	IsString,
	IsUUID,
	Matches,
} from 'class-validator';

export class CreateArticleDto implements Article {
	id: string;

	@IsString()
	@IsNotEmpty()
	title: string;

	@IsString()
	@IsNotEmpty()
	description: string;

	@IsString()
	@IsNotEmpty()
	content: string;

	@IsString()
	@IsNotEmpty()
	@Matches(/^[a-z0-9]+(?:(?:-|_)+[a-z0-9]+)*$/)
	slug: string;

	@IsOptional()
	@IsString()
	@IsNotEmpty()
	image: string;

	@IsOptional()
	@IsDateString()
	publishedAt: Date;

	@IsOptional()
	@IsUUID('4')
	categoryId: string;

	@IsOptional()
	@IsUUID('4')
	authorId: string;
}
