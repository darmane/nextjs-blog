# Next.js Blog

Monorepo to build a web site including a blog, with authentication and backend connection already configured.

## Stack

- Next.js: Dashboard
- ChakraUI: Styles
- NestJS: Backend
- Passport.js: Authentication
- Typescript

## License

MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" basis and without warranty of any kind. Use it at your own risk.