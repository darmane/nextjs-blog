import { NavMenuState, initialState } from '.';

// Actions
const OPEN_MENU = 'OPEN_MENU';
const CLOSE_MENU = 'CLOSE_MENU';

// Action types
interface OpenMenu {
	type: typeof OPEN_MENU;
}

interface CloseMenu {
	type: typeof CLOSE_MENU;
}

export type NavMenuActions = OpenMenu | CloseMenu;

const alertsReducer = (
	state: NavMenuState = initialState,
	action: NavMenuActions
) => {
	switch (action.type) {
		case 'OPEN_MENU':
			return {
				...state,
				isOpen: true,
			};
		case 'CLOSE_MENU':
			return {
				...state,
				isOpen: false,
			};
		default:
			return state;
	}
};

export default alertsReducer;
