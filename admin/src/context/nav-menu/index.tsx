import React, { createContext, useContext, useReducer } from 'react';
import alertsReducer, { NavMenuActions } from './reducer';

export interface NavMenuState {
	isOpen: boolean;
}

export const initialState: NavMenuState = {
	isOpen: false,
};

const NavMenuContext = createContext<{
	state: NavMenuState;
	dispatch: React.Dispatch<NavMenuActions>;
}>({ state: initialState, dispatch: () => null });

export const useNavMenu = () => {
	const { state, dispatch } = useContext(NavMenuContext);

	const open = () => dispatch({ type: 'OPEN_MENU' });

	const close = () => dispatch({ type: 'CLOSE_MENU' });

	return { isOpen: state.isOpen, open, close };
};

export const NavMenuProvider = ({ children }: Props) => {
	const [state, dispatch] = useReducer(alertsReducer, initialState);

	return (
		<NavMenuContext.Provider value={{ state, dispatch }}>
			{children}
		</NavMenuContext.Provider>
	);
};

interface Props {
	children: React.ReactNode;
}

export default NavMenuContext;
