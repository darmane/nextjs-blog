import React, { createContext, useContext, useReducer } from 'react';
import { AlertStatus } from '@chakra-ui/react';
import alertsReducer, { AlertsActions } from './reducer';
import { v4 as uuid } from 'uuid';

export interface IAlert {
	id?: string;
	status: AlertStatus;
	title: string;
	description: string;
}

export interface AlertsState {
	alerts: IAlert[];
}

export const initialState: AlertsState = {
	alerts: [],
};

const AlertsContext = createContext<{
	state: AlertsState;
	dispatch: React.Dispatch<AlertsActions>;
}>({ state: initialState, dispatch: () => null });

export const useAlerts = () => {
	const { state, dispatch } = useContext(AlertsContext);

	const addAlert = async (alert: IAlert) => {
		let id: string;
		if (alert.id) {
			id = alert.id;
			const foundAlert = state.alerts.find(alert => alert.id === id);
			if (foundAlert) {
				return;
			}
		} else {
			id = uuid();
		}

		dispatch({ type: 'ADD_ALERT', payload: { ...alert, id: id } });

		setTimeout(() => {
			dispatch({ type: 'REMOVE_ALERT', payload: id });
		}, 3000);
	};

	const removeAlert = (alertId: string) => {
		dispatch({ type: 'REMOVE_ALERT', payload: alertId });
	};

	return { alerts: state.alerts, addAlert, removeAlert };
};

export const AlertsProvider = ({ children }: Props) => {
	const [state, dispatch] = useReducer(alertsReducer, initialState);

	return (
		<AlertsContext.Provider value={{ state, dispatch }}>
			{children}
		</AlertsContext.Provider>
	);
};

interface Props {
	children: React.ReactNode;
}

export default AlertsContext;
