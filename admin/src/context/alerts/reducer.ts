import { v4 as uuid } from 'uuid';
import { IAlert, AlertsState, initialState } from '.';

// Actions
const ADD_ALERT = 'ADD_ALERT';
const REMOVE_ALERT = 'REMOVE_ALERT';

// Action types
interface AddAlert {
	type: typeof ADD_ALERT;
	payload: IAlert;
}

interface RemoveAlert {
	type: typeof REMOVE_ALERT;
	payload: string;
}

export type AlertsActions = AddAlert | RemoveAlert;

const alertsReducer = (
	state: AlertsState = initialState,
	action: AlertsActions
) => {
	switch (action.type) {
		case 'ADD_ALERT':
			const id = action.payload.id ? action.payload.id : uuid();
			return {
				...state,
				alerts: [...state.alerts, { ...action.payload, id: id }],
			};
		case 'REMOVE_ALERT':
			return {
				...state,
				alerts: state.alerts.filter(
					alert => alert.id !== action.payload
				),
			};
		default:
			return state;
	}
};

export default alertsReducer;
