import { AlertsProvider } from './alerts';
import { NavMenuProvider } from './nav-menu';

const AppContextProvider = ({ children }: Props) => {
	return (
		<AlertsProvider>
			<NavMenuProvider>{children}</NavMenuProvider>
		</AlertsProvider>
	);
};

interface Props {
	children: React.ReactNode;
}

export default AppContextProvider;
