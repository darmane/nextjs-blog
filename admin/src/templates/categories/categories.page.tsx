import { useMemo, useEffect } from 'react';
import { User } from '@prisma/client';
import useAxiosSWR from '@/hooks/useAxiosSWR';
import { useAlerts } from '@/context/alerts';
import { useRouter } from 'next/router';
import { Column } from 'react-table';
import AdminLayout from '@/layouts/admin-layout';
import { Heading } from '@chakra-ui/react';
import Table from '@/components/admin/table';
import apiFetcher from '@/lib/apiFetcher';

const Categories = () => {
	const { addAlert } = useAlerts();
	const { data: categories, error } = useAxiosSWR('/api/categories');
	const router = useRouter();

	useEffect(() => {
		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.statusText,
				status: 'error',
			});
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [error]);

	const columns = useMemo<Column<any>[]>(
		() => [
			{
				Header: 'Name',
				accessor: 'name',
			},
			{
				Header: 'Slug',
				accessor: 'slug',
			},
		],
		[]
	);

	const handleCreateCategory = () => {
		router.push('/admin/categories/create');
	};

	const handleUpdateCategory = (id: string) => {
		router.push(`/admin/categories/${id}`);
	};

	const handleDeleteCategory = async (id: string) => {
		const { error } = await apiFetcher(`/categories/${id}`, 'DELETE');

		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.data.message,
				status: 'error',
			});
		} else {
			router.reload();
		}
	};

	return (
		<>
			<Heading as='h1'>Categories</Heading>
			<Table
				title={'Categories'}
				data={categories ?? []}
				columns={columns}
				onCreateRow={handleCreateCategory}
				onDeleteRow={handleDeleteCategory}
				onUpdateRow={handleUpdateCategory}
			/>
		</>
	);
};

Categories.getLayout = AdminLayout;

export interface Props {
	categories: User[];
}

export default Categories;
