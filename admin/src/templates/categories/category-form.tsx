import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import getSlug from '@/lib/getSlug';
import {
	FormControl,
	FormLabel,
	FormErrorMessage,
	Input,
	Button,
	VStack,
	HStack,
	Image,
	InputRightElement,
	InputGroup,
	Icon,
} from '@chakra-ui/react';
import { Category } from '@prisma/client';
import { FaRedo } from 'react-icons/fa';
import { useAlerts } from '@/context/alerts';

export type FormFields = {
	name: string;
	slug: string;
	image: FileList;
};

const CategoryForm = ({ category, onSubmit }: Props) => {
	const router = useRouter();
	const { addAlert } = useAlerts();
	const [imagePreview, setImagePreview] = useState<string>(
		'/images/placeholder.png'
	);

	const {
		handleSubmit,
		register,
		setError,
		clearErrors,
		watch,
		setValue,
		formState: { errors, isSubmitting },
	} = useForm<FormFields>();

	useEffect(() => {
		category?.image && setImagePreview(category.image);
	}, [category]);

	const handleCancel = () => {
		router.back();
	};

	const handleGenerateSlug = () => {
		const name = watch('name');
		const slug = getSlug(name);

		if (!name) {
			addAlert({
				status: 'info',
				title: '',
				description:
					// eslint-disable-next-line quotes
					"Field 'Name' must have a value before generate a slug.",
			});

			return;
		}

		setValue('slug', slug, { shouldValidate: true });
	};

	const handleImageOnChange = (event: React.FormEvent<HTMLInputElement>) => {
		clearErrors('image');

		const files = event.currentTarget.files;
		if (!files || files.length < 1) {
			setError('image', {
				type: 'custom',
				message: 'Only one file can be uploaded at a time.',
			});
			event.currentTarget.files = null;
			event.currentTarget.value = '';
			return;
		}

		const file = files[0];
		if (file.size > 2000000) {
			setError('image', {
				type: 'custom',
				message: 'Image size must be less than 2Mb.',
			});
			event.currentTarget.files = null;
			event.currentTarget.value = '';
			return;
		}

		const previewUrl = URL.createObjectURL(file);
		setImagePreview(previewUrl);
	};

	return (
		<VStack
			as='form'
			onSubmit={handleSubmit(onSubmit)}
			spacing='4'
			bgColor='white'
			rounded='xl'
			padding='4'
			// encType='multipart/form-data'
		>
			<FormControl isInvalid={errors.name !== undefined}>
				<FormLabel htmlFor='name'>Name</FormLabel>
				<Input
					id='name'
					placeholder='Technology'
					type='text'
					defaultValue={category?.name}
					{...register('name', {
						required: 'This is required',
						minLength: {
							value: 2,
							message: 'Minimum length should be 2',
						},
					})}
				/>
				<FormErrorMessage>
					{errors.name && errors.name.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.slug !== undefined}>
				<FormLabel htmlFor='slug'>Slug</FormLabel>
				<InputGroup size='md'>
					<Input
						id='slug'
						placeholder='category-slug'
						type='slug'
						defaultValue={category?.slug}
						{...register('slug', {
							required: 'This is required',
							pattern: {
								value: /^[a-z0-9]+(?:-[a-z0-9]+)*$/,
								message:
									// eslint-disable-next-line quotes
									"Must be only numbers, lower case letters and '-'",
							},
						})}
					/>
					<InputRightElement marginRight='2'>
						<Button
							h='1.75rem'
							size='sm'
							onClick={handleGenerateSlug}
						>
							<Icon as={FaRedo} />
						</Button>
					</InputRightElement>
				</InputGroup>
				<FormErrorMessage>
					{errors.slug && errors.slug.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.image !== undefined}>
				<FormLabel htmlFor='image'>Image</FormLabel>
				<Input
					id='image'
					type='file'
					accept='image/*'
					{...register('image', {})}
					onChange={handleImageOnChange}
				/>
				<FormErrorMessage>
					{errors.image && errors.image.message}
				</FormErrorMessage>
			</FormControl>
			<Image src={imagePreview} alt='Category image preview' width='60' />
			<HStack paddingTop='4'>
				<Button
					isLoading={isSubmitting}
					colorScheme='gray'
					type='button'
					onClick={handleCancel}
				>
					Back
				</Button>
				<Button
					isLoading={isSubmitting}
					colorScheme='twitter'
					type='submit'
				>
					Submit
				</Button>
			</HStack>
		</VStack>
	);
};

interface Props {
	category?: Category;
	onSubmit: (values: FormFields) => void;
}

export default CategoryForm;
