import apiFetcher from '@/lib/apiFetcher';
import { GetServerSideProps } from 'next';
import { Props } from './[id].page';

const getServerSideProps: GetServerSideProps<Props> = async context => {
	try {
		const { id } = context.query;
		const category = await apiFetcher(`/categories/${id}`, 'GET');

		return {
			props: {
				category: category.data,
				hasError: category.error != null,
			},
		};
	} catch (error) {
		console.log(error);
		return {
			props: {
				hasError: true,
			},
		};
	}
};

export default getServerSideProps;
