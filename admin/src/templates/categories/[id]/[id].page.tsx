import { useAlerts } from '@/context/alerts';
import AdminLayout from '@/layouts/admin-layout';
import apiFetcher from '@/lib/apiFetcher';
import CategoryForm from '../category-form';
import { FormFields } from '../category-form';
import { Category } from '@prisma/client';

const UpdateCategory = ({ category, hasError }: Props) => {
	const { addAlert } = useAlerts();

	const handleOnSubmit = async (values: FormFields) => {
		const { image: files, ...restValues } = values;

		let image: string | undefined;
		if (files.length > 0) {
			const formData = new FormData();
			for (let i = 0; i < files.length; i++) {
				formData.append('files', files[i]);
			}

			const { data: uploadData, error: uploadError } = await apiFetcher(
				'/uploads',
				'POST',
				formData
			);

			if (uploadError || uploadData.length <= 0) {
				addAlert({
					title: uploadError.response.status,
					description: 'Error uploading image.',
					status: 'error',
				});

				return;
			}

			image = `/${uploadData[0].path}`;
		}

		const { error } = await apiFetcher(
			'/categories',
			'POST',
			image
				? {
						...restValues,
						image: image,
				  }
				: restValues
		);

		if (error) {
			addAlert({
				title: error.response.status,
				description: `${error.response.statusText}. Slug may already exists.`,
				status: 'error',
			});

			return;
		}

		addAlert({
			title: '201',
			description: 'Category updated.',
			status: 'success',
		});
	};

	return <CategoryForm category={category} onSubmit={handleOnSubmit} />;
};

export interface Props {
	category?: Category;
	hasError?: boolean;
}

UpdateCategory.getLayout = AdminLayout;

export default UpdateCategory;
