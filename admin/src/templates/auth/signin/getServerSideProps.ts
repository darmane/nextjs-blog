import { GetServerSideProps } from 'next';

const getServerSideProps: GetServerSideProps = async context => {
	const session = true;

	if (session) {
		return {
			redirect: {
				destination: '/admin',
				permanent: false,
			},
			props: {},
		};
	} else {
		return {
			props: {},
		};
	}
};

export default getServerSideProps;
