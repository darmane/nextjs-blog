import { useState } from 'react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import {
	FormErrorMessage,
	FormLabel,
	FormControl,
	Input,
	Button,
	InputRightElement,
	InputGroup,
	Stack,
	VStack,
	Text,
	Heading,
	Image,
	Flex,
} from '@chakra-ui/react';
import { useAlerts } from 'src/context/alerts';
import AuthLayout from '@/layouts/auth-layout';

type FormData = {
	email: string;
	password: string;
};

const SignIn = () => {
	const [showPassword, setShowPassword] = useState(false);

	const router = useRouter();

	const { addAlert } = useAlerts();

	const {
		handleSubmit,
		register,
		setError,
		formState: { errors, isSubmitting },
	} = useForm<FormData>();

	const handleShowPassword = () => setShowPassword(!showPassword);

	const onSubmit = async (values: FormData) => {
		const res: any = undefined;

		if (res.error) {
			if (res.error === 'CredentialsSignin') {
				setError(
					'email',
					{ type: 'custom', message: 'Possibly incorrect email' },
					{ shouldFocus: true }
				);
				setError(
					'password',
					{ type: 'custom', message: 'Possibly incorrect password' },
					{ shouldFocus: true }
				);
			} else {
				addAlert({
					status: 'error',
					title: 'Sign in error',
					description: res.error,
				});
			}
		} else {
			router.reload();
		}
	};

	return (
		<Flex
			flexDirection='column'
			justifyContent='center'
			alignItems='center'
			minHeight='95vh'
		>
			<VStack spacing='8'>
				<VStack>
					<Heading as='h1' textAlign='center'>
						Sign in to your account
					</Heading>
					<Text textAlign='center'>to reach the whole world! 🌍</Text>
				</VStack>
				<Stack
					as='form'
					onSubmit={handleSubmit(onSubmit)}
					spacing='10'
					padding='8'
					rounded='lg'
					boxShadow='lg'
					bgColor='white'
					width='full'
				>
					<Stack spacing='4'>
						<FormControl isInvalid={errors.email !== undefined}>
							<FormLabel htmlFor='email'>Email</FormLabel>
							<Input
								id='email'
								placeholder='user@email.com'
								type='email'
								{...register('email', {
									required: 'This is required',
									minLength: {
										value: 4,
										message: 'Minimum length should be 4',
									},
								})}
							/>
							<FormErrorMessage>
								{errors.email && errors.email.message}
							</FormErrorMessage>
						</FormControl>
						<FormControl isInvalid={errors.password !== undefined}>
							<FormLabel htmlFor='password'>Password</FormLabel>
							<InputGroup size='md'>
								<Input
									id='password'
									placeholder='yoursupersecret'
									type={showPassword ? 'text' : 'password'}
									{...register('password', {
										required: 'This is required',
										minLength: {
											value: 4,
											message:
												'Minimum length should be 4',
										},
									})}
								/>
								<InputRightElement width='4.5rem'>
									<Button
										size='sm'
										onClick={handleShowPassword}
									>
										{showPassword ? 'Hide' : 'Show'}
									</Button>
								</InputRightElement>
							</InputGroup>
							<FormErrorMessage>
								{errors.password && errors.password.message}
							</FormErrorMessage>
						</FormControl>
					</Stack>
					<Button
						marginTop='10'
						colorScheme='twitter'
						isLoading={isSubmitting}
						type='submit'
					>
						Sign in
					</Button>
				</Stack>
				<Image
					src='/images/scandi-273.png'
					alt='Sailboat'
					boxSize='150px'
					objectFit='cover'
				/>
			</VStack>
		</Flex>
	);
};

SignIn.getLayout = AuthLayout;

export default SignIn;
