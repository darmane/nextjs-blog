import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useAlerts } from '@/context/alerts';
import useAxiosSWR from '@/hooks/useAxiosSWR';
import AdminLayout from '@/layouts/admin-layout';
import UserForm from './user-form';
import apiFetcher from '@/lib/apiFetcher';
import { FormData } from './user-form';

const UpdateUser = () => {
	const router = useRouter();

	const { addAlert } = useAlerts();

	const { data, error } = useAxiosSWR(`/api/users/${router.query.pid}`);

	useEffect(() => {
		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.statusText,
				status: 'error',
			});
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [error]);

	const handleOnSubmit = async (values: FormData) => {
		let key: keyof typeof values;
		for (key in values) {
			(values[key] === '' || values[key] == null) && delete values[key];
		}

		const { error } = await apiFetcher(
			`/users/${router.query.pid}`,
			'PUT',
			values
		);

		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.statusText,
				status: 'error',
			});
		} else {
			addAlert({
				title: '200',
				description: 'User updated.',
				status: 'success',
			});
		}
	};

	return <UserForm user={data} onSubmit={handleOnSubmit} />;
};

UpdateUser.getLayout = AdminLayout;

export default UpdateUser;
