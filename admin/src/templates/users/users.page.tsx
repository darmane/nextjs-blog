import { useMemo, useEffect } from 'react';
import { User } from '@prisma/client';
import useAxiosSWR from '@/hooks/useAxiosSWR';
import { useAlerts } from '@/context/alerts';
import { useRouter } from 'next/router';
import { Column } from 'react-table';
import AdminLayout from '@/layouts/admin-layout';
import { Avatar, Badge, Flex, Heading, Text } from '@chakra-ui/react';
import Table from '@/components/admin/table';
import apiFetcher from '@/lib/apiFetcher';

const Users = () => {
	const { addAlert } = useAlerts();
	const { data: users, error } = useAxiosSWR('/api/users');
	const router = useRouter();

	useEffect(() => {
		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.statusText,
				status: 'error',
			});
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [error]);

	const columns = useMemo<Column<any>[]>(
		() => [
			{
				Header: 'User',
				accessor: 'name',
				Cell: ({ row }) => {
					const { image, name } = row.original;
					return (
						<Flex alignItems='center' gap='2'>
							<Avatar size={'md'} src={image} />
							<Text>{name}</Text>
						</Flex>
					);
				},
			},
			{
				Header: 'Email',
				accessor: 'email',
			},
			{
				Header: 'Verified',
				accessor: 'emailVerified',
				Cell: ({ value }) =>
					value ? (
						<Badge colorScheme='green'>Verified</Badge>
					) : (
						<Badge colorScheme='orange'>Pending</Badge>
					),
			},
			{
				Header: 'Blocked',
				accessor: 'isBlocked',
				Cell: ({ value }) =>
					value ? (
						<Badge colorScheme='gray'>Blocked</Badge>
					) : (
						<Badge colorScheme='green'>Active</Badge>
					),
			},
			{
				Header: 'Role',
				accessor: 'role',
			},
		],
		[]
	);

	const handleCreateRow = () => {
		router.push('/admin/users/create');
	};

	const handleDeleteRow = async (id: string) => {
		const { error } = await apiFetcher(`/users/${id}`, 'DELETE');

		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.data.message,
				status: 'error',
			});
		} else {
			addAlert({
				title: '200',
				description: 'User blocked.',
				status: 'success',
			});
		}
	};

	const handleUpdateRow = (id: string) => {
		router.push(`/admin/users/${id}`);
	};

	return (
		<>
			<Heading as='h1'>Users</Heading>
			<Table
				title={'Users'}
				data={users ?? []}
				columns={columns}
				onCreateRow={handleCreateRow}
				onDeleteRow={handleDeleteRow}
				onUpdateRow={handleUpdateRow}
			/>
		</>
	);
};

Users.getLayout = AdminLayout;

export interface Props {
	users: User[];
}

export default Users;
