import { useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import { useForm } from 'react-hook-form';
import {
	FormControl,
	FormLabel,
	FormErrorMessage,
	Input,
	Button,
	VStack,
	RadioGroup,
	Radio,
	HStack,
	InputGroup,
	InputRightElement,
	Switch,
	Avatar,
} from '@chakra-ui/react';
import { User } from '@prisma/client';

export type FormData = {
	image: string;
	name: string;
	email: string;
	password?: string;
	role: 'ADMIN' | 'AUTHOR';
};

const UserForm = ({ user, onSubmit }: Props) => {
	const [showPassword, setShowPassword] = useState(false);

	const { data: sessionUser } = useSession();

	const router = useRouter();

	const isUpdating = useMemo(() => user !== undefined, [user]);

	const {
		handleSubmit,
		register,
		formState: { errors, isSubmitting },
	} = useForm<FormData>();

	const handleShowPassword = () => setShowPassword(!showPassword);

	const handleCancel = () => {
		router.back();
	};

	return (
		<VStack
			as='form'
			onSubmit={handleSubmit(onSubmit)}
			spacing='4'
			bgColor='white'
			rounded='xl'
			padding='4'
		>
			<FormControl isInvalid={errors.image !== undefined}>
				<Avatar
					id='image'
					size={'lg'}
					src={user?.image!}
					cursor='pointer'
				/>
				<Input
					id='image'
					type='text'
					{...register('image')}
					display='none'
				/>
				<FormErrorMessage>
					{errors.image && errors.image.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.name !== undefined}>
				<FormLabel htmlFor='name'>Name</FormLabel>
				<Input
					id='name'
					placeholder='George Lucas'
					type='text'
					defaultValue={user?.name ?? undefined}
					{...register('name', {
						required: {
							message: 'This is required',
							value: !isUpdating,
						},
						minLength: {
							value: 2,
							message: 'Minimum length should be 2',
						},
					})}
				/>
				<FormErrorMessage>
					{errors.name && errors.name.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.email !== undefined}>
				<FormLabel htmlFor='email'>Email</FormLabel>
				<Input
					id='email'
					placeholder='georgelucas@email.com'
					type='email'
					defaultValue={user?.email ?? undefined}
					{...register('email', {
						required: {
							message: 'This is required',
							value: !isUpdating,
						},
						minLength: {
							value: 4,
							message: 'Minimum length should be 4',
						},
					})}
				/>
				<FormErrorMessage>
					{errors.email && errors.email.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.password !== undefined}>
				<FormLabel htmlFor='password'>Password</FormLabel>
				<InputGroup size='md'>
					<Input
						id='password'
						placeholder='yoursupersecret'
						type={showPassword ? 'text' : 'password'}
						{...register('password', {
							required: {
								message: 'This is required',
								value: !isUpdating,
							},
							minLength: {
								value: 4,
								message: 'Minimum length should be 4',
							},
						})}
					/>
					<InputRightElement width='4.5rem'>
						<Button size='sm' onClick={handleShowPassword}>
							{showPassword ? 'Hide' : 'Show'}
						</Button>
					</InputRightElement>
				</InputGroup>
				<FormErrorMessage>
					{errors.password && errors.password.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl isInvalid={errors.role !== undefined}>
				<FormLabel htmlFor='role'>Role</FormLabel>
				<RadioGroup
					defaultValue={user?.role}
					isDisabled={sessionUser?.user.role !== 'ADMIN'}
				>
					<HStack spacing='4'>
						<Radio
							value='ADMIN'
							{...register('role', {
								required: {
									message: 'This is required',
									value: !isUpdating,
								},
							})}
						>
							Admin
						</Radio>
						<Radio
							value='AUTHOR'
							{...register('role', {
								required: {
									message: 'This is required',
									value: !isUpdating,
								},
							})}
						>
							Author
						</Radio>
					</HStack>
				</RadioGroup>
				<FormErrorMessage>
					{errors.role && errors.role.message}
				</FormErrorMessage>
			</FormControl>
			<FormControl>
				<FormLabel htmlFor='isBlocked'>Is Blocked</FormLabel>
				<Switch
					colorScheme='red'
					defaultChecked={user?.isBlocked === true}
				/>
			</FormControl>
			<HStack paddingTop='4'>
				<Button
					isLoading={isSubmitting}
					colorScheme='gray'
					type='button'
					onClick={handleCancel}
				>
					Back
				</Button>
				<Button
					isLoading={isSubmitting}
					colorScheme='twitter'
					type='submit'
				>
					Submit
				</Button>
			</HStack>
		</VStack>
	);
};

interface Props {
	user?: User;
	onSubmit: (values: FormData) => void;
}

export default UserForm;
