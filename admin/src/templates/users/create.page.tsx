import { useRouter } from 'next/router';
import { useAlerts } from '@/context/alerts';
import AdminLayout from '@/layouts/admin-layout';
import apiFetcher from '@/lib/apiFetcher';
import UserForm from './user-form';
import { FormData } from './user-form';

const CreateUser = () => {
	const { addAlert } = useAlerts();

	const router = useRouter();

	const handleOnSubmit = async (values: FormData) => {
		!values.password && delete values.password;

		const { error } = await apiFetcher('/users', 'POST', values);

		if (error) {
			addAlert({
				title: error.response.status,
				description: error.response.statusText,
				status: 'error',
			});
		} else {
			addAlert({
				title: '200',
				description: 'User created.',
				status: 'success',
			});

			router.back();
		}
	};

	return <UserForm onSubmit={handleOnSubmit} />;
};

CreateUser.getLayout = AdminLayout;

export default CreateUser;
