import { Heading, Box } from '@chakra-ui/react';
import AdminLayout from '@/layouts/admin-layout';

const Posts = () => {
	return (
		<>
			<Heading as='h1'>Articles</Heading>
			<Box flexGrow='1'>HELLO WORLD ...</Box>
		</>
	);
};

Posts.getLayout = AdminLayout;

export default Posts;
