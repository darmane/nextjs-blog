import { Heading, Box } from '@chakra-ui/react';
import AdminLayout from '@/layouts/admin-layout';
import apiFetcher from '@/lib/apiFetcher';

const Home = () => {
	apiFetcher('/', 'GET').then(value => console.log(value));

	return (
		<>
			<Heading as='h1'>Dashboard</Heading>
			<Box flexGrow='1'>HELLO WORLD ...</Box>
		</>
	);
};

Home.getLayout = AdminLayout;

export default Home;
