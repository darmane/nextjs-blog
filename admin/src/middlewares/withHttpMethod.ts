import { NextApiRequest, NextApiResponse } from 'next';

const withHttpMethod = (httpmMethod: 'GET' | 'POST' | 'PUT' | 'DELETE') => {
	return async function (req: NextApiRequest, res: NextApiResponse) {
		try {
			if (req.method !== httpmMethod) {
				return res.status(405).json({ message: 'Method Not Allowed.' });
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json({ message: 'Internal Server Error.' });
		}
	};
};

export default withHttpMethod;
