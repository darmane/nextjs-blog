import { NextApiRequest, NextApiResponse } from 'next';

const withForbiddenProps = (accessors: string[]) => {
	return async function (req: NextApiRequest, res: NextApiResponse) {
		try {
			const body = req.body;

			for (const accessor of accessors) {
				const value = body[accessor];
				if (value) {
					delete req.body[accessor];
				}
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json({ message: 'Internal Server Error.' });
		}
	};
};

export default withForbiddenProps;
