import { NextApiRequest, NextApiResponse } from 'next';
import { getToken } from 'next-auth/jwt';

const secret = process.env.SECRET;

const withAuth = async (
	req: NextApiRequest,
	res: NextApiResponse,
	next?: any
) => {
	try {
		const token = await getToken({
			req,
			secret: secret,
		});

		if (!token) {
			return res.status(401).json({ message: 'Unauthorized.' });
		}

		if (!token.role || !token.id) {
			return res.status(403).json({ message: 'Forbidden.' });
		}

		req.jwt = token;

		next && next();
	} catch (error) {
		console.log(error);
		return res.status(500).json({ message: 'Internal Server Error.' });
	}
};

export default withAuth;
