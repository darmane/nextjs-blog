import { NextApiRequest, NextApiResponse } from 'next';

const withRequiredProps = (...accessors: string[]) => {
	return async function (req: NextApiRequest, res: NextApiResponse) {
		try {
			const body = req.body;

			if (!body) {
				return res.status(400).json({ message: 'Bad Request.' });
			}

			for (const accessor of accessors) {
				const value = body[accessor];
				if (!value) {
					return res.status(400).json({ message: 'Bad Request.' });
				}
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json({ message: 'Internal Server Error.' });
		}
	};
};

export default withRequiredProps;
