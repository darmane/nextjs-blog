import withMiddleware from '@/lib/withMiddleware';
import { withRoleGuard, withAuth } from './index';

const withAdmin = withMiddleware(withAuth, withRoleGuard(['ADMIN']));

export default withAdmin;
