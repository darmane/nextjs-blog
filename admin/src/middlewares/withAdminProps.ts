import { NextApiRequest, NextApiResponse } from 'next';

const withAdminProps = (...accessors: string[]) => {
	return async function (req: NextApiRequest, res: NextApiResponse) {
		try {
			const body = req.body;
			const token = req.jwt;

			if (token?.role !== 'ADMIN') {
				for (const accessor of accessors) {
					if (body[accessor]) {
						delete req.body[accessor];
					}
				}
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json({ message: 'Internal Server Error.' });
		}
	};
};

export default withAdminProps;
