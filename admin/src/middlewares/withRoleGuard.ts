import { NextApiRequest, NextApiResponse } from 'next';

const withRoleGuard = (roles: string[]) => {
	return async function (req: NextApiRequest, res: NextApiResponse) {
		try {
			if (!req.jwt?.role || !roles.includes(req.jwt.role)) {
				return res.status(403).json({ message: 'Forbidden.' });
			}
		} catch (error) {
			console.log(error);
			return res.status(500).json({ message: 'Internal Server Error.' });
		}
	};
};

export default withRoleGuard;
