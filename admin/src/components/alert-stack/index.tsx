import { useAlerts } from 'src/context/alerts';
import Alert from '@/components/alert';
import { Flex, VStack } from '@chakra-ui/react';

const AlertStack = () => {
	const { alerts } = useAlerts();

	return (
		<Flex
			position='fixed'
			width='full'
			zIndex='tooltip'
			paddingTop='4'
			justifyContent='center'
		>
			<VStack maxWidth='xl'>
				{alerts.map(alert => (
					<Alert key={alert.id} {...alert} />
				))}
			</VStack>
		</Flex>
	);
};

export default AlertStack;
