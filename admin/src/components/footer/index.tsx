import { useMemo } from 'react';
import {
	VStack,
	Text,
	Divider,
	Container,
	ContainerProps,
} from '@chakra-ui/react';

const Footer = ({ children, ...props }: Props) => {
	const currentYear = useMemo(() => new Date().getFullYear(), []);

	return (
		<Container {...props}>
			{children}
			<VStack>
				<Divider />
				<Text>© {currentYear} - Coded with ❤️ by Confused</Text>
			</VStack>
		</Container>
	);
};

interface Props extends ContainerProps {
	children?: React.ReactNode;
}

export default Footer;
