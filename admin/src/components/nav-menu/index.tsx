import {
	Flex,
	Stack,
	Image,
	Spacer,
	IconButton,
	useColorModeValue,
} from '@chakra-ui/react';
import { CloseIcon } from '@chakra-ui/icons';
import { RiTeamLine, RiDashboardLine } from 'react-icons/ri';
import { BsNewspaper } from 'react-icons/bs';
import { BiCategoryAlt } from 'react-icons/bi';
import NavLink from './navlink';
import { useNavMenu } from '@/context/nav-menu';

const NavMenu = () => {
	const { isOpen, close } = useNavMenu();

	return (
		<Flex
			bg={useColorModeValue('white', 'gray.700')}
			width={{ base: 'full', md: '80' }}
			height='100vh'
			padding={{ base: 4, md: 6 }}
			flexDirection='column'
			position='fixed'
			zIndex='overlay'
			transition='0.5s ease-in-out'
			transform={{
				base: `translateX(${isOpen ? '0%' : '-100%'})`,
				md: 'unset',
			}}
		>
			<Flex
				alignItems='center'
				justifyContent='space-between'
				paddingBottom={{ base: 4, md: 6 }}
			>
				<IconButton
					icon={<CloseIcon />}
					aria-label='Close navigation menu.'
					display={{ base: 'unset', md: 'none' }}
					onClick={close}
					bgColor='transparent'
				/>
				<Image
					src='/images/scandi-273.png'
					alt='Dashboard logo'
					width='12'
				/>
			</Flex>
			<Stack as='nav' spacing='2' paddingY='4'>
				<NavLink
					label='Dashboard'
					href='/admin'
					icon={RiDashboardLine}
				/>
				<NavLink
					label='Articles'
					href='/admin/articles'
					icon={BsNewspaper}
				/>
				<NavLink
					label='Categories'
					href='/admin/categories'
					icon={BiCategoryAlt}
				/>
				<NavLink label='Users' href='/admin/users' icon={RiTeamLine} />
			</Stack>
			<Spacer />
		</Flex>
	);
};

export default NavMenu;
