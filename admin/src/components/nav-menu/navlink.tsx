import Link from '@/components/link';
import { Flex, Icon, Spacer, Text, useColorModeValue } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import { IconType } from 'react-icons';
import { ChevronRightIcon } from '@chakra-ui/icons';

const NavLink = ({ href, label, icon }: Props) => {
	const { pathname } = useRouter();

	const isActive = useMemo(() => pathname === href, [pathname, href]);

	const activeBackgroundColor = useColorModeValue('black', 'whiteAlpha.700');

	const activeColor = useColorModeValue('white', 'black');

	return (
		<Link href={href}>
			<Flex
				gap='4'
				paddingX='4'
				paddingY='3'
				alignItems='center'
				rounded='xl'
				boxShadow={isActive ? 'md' : 'none'}
				bgColor={isActive ? activeBackgroundColor : 'transparent'}
				color={isActive ? activeColor : 'unset'}
				cursor='pointer'
				transition='0.1s ease-in-out'
			>
				<Icon as={icon} width='5' height='5' />
				<Text>{label}</Text>
				<Spacer />
				<ChevronRightIcon />
			</Flex>
		</Link>
	);
};

interface Props {
	href: string;
	label: string;
	icon: IconType;
}

export default NavLink;
