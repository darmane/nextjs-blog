import {
	Flex,
	Button,
	Menu,
	MenuButton,
	MenuList,
	MenuDivider,
	MenuItem,
	Avatar,
	Center,
	useColorModeValue,
	useColorMode,
	IconButton,
	Text,
	Icon,
} from '@chakra-ui/react';
import { MoonIcon, SunIcon, HamburgerIcon } from '@chakra-ui/icons';
import { BiLogOut } from 'react-icons/bi';
import { useNavMenu } from '@/context/nav-menu';
import Searchbar from '@/components/searchbar';

const Navbar = () => {
	const { toggleColorMode } = useColorMode();
	const { data: sessionUser } = { data: undefined };
	const { open } = useNavMenu();

	const handleSignOut = () => {};

	return (
		<Flex
			alignItems='center'
			justifyContent='space-between'
			bgColor={useColorModeValue('white', 'gray.700')}
			paddingX={{ base: '4', md: '12' }}
			paddingY={{ base: '4', md: '6' }}
			position='sticky'
			zIndex='sticky'
			top='0'
			boxShadow='xs'
			marginLeft={{ base: 0, md: 80 }}
			borderLeft={{ md: 'solid 1px' }}
			borderColor={{
				md: useColorModeValue('blackAlpha.50', 'blackAlpha.900'),
			}}
		>
			<IconButton
				icon={<HamburgerIcon />}
				aria-label='Open mobile navigation menu'
				onClick={open}
				display={{ base: 'unset', md: 'none' }}
				bgColor='transparent'
			/>
			<Searchbar maxWidth='sm' display={{ base: 'none', md: 'unset' }} />
			<Flex alignItems={'center'} gap={{ base: '4', md: '6' }}>
				<IconButton
					colorScheme={useColorModeValue('purple', 'orange')}
					icon={useColorModeValue(<MoonIcon />, <SunIcon />)}
					aria-label='Dark theme switch'
					onClick={toggleColorMode}
				/>
				<Menu>
					<MenuButton
						as={Button}
						rounded={'full'}
						variant={'link'}
						cursor={'pointer'}
						minWidth='0'
						minHeight='0'
					>
						<Avatar size={'md'} src={sessionUser?.user.image!} />
					</MenuButton>
					<MenuList alignItems={'center'} as='nav'>
						<Center marginY='6'>
							<Avatar
								size={'2xl'}
								src={sessionUser?.user.image!}
							/>
						</Center>
						<Center>
							<p>{sessionUser?.user.name ?? 'User'}</p>
						</Center>
						<br />
						<MenuDivider />
						<MenuItem>Account Settings</MenuItem>
						<MenuItem
							as={Flex}
							onClick={handleSignOut}
							gap='4'
							cursor='pointer'
						>
							<Text>Sign out</Text>
							<Icon as={BiLogOut} width='5' height='5' />
						</MenuItem>
					</MenuList>
				</Menu>
			</Flex>
		</Flex>
	);
};

export default Navbar;
