import { useMemo } from 'react';
import {
	Table as ChakraTable,
	Thead,
	Tbody,
	Tr,
	Th,
	Td,
	TableCaption,
	TableContainer,
	HStack,
	IconButton,
	Icon,
	Button,
	useColorModeValue,
	Flex,
	Heading,
	Spacer,
	Box,
} from '@chakra-ui/react';
import { Column, useTable } from 'react-table';
import { BiTrash, BiEdit, BiPlus, BiFilter } from 'react-icons/bi';
import Searchbar from '../searchbar';

const Table = ({
	title,
	data,
	columns,
	onCreateRow,
	onDeleteRow,
	onUpdateRow,
	tableCaption,
}: Props) => {
	const { headers, rows, prepareRow } = useTable({
		columns,
		data,
	});

	const hasActions = useMemo(
		() => onDeleteRow !== undefined || onUpdateRow !== undefined,
		[onDeleteRow, onUpdateRow]
	);

	return (
		<Flex
			bgColor={useColorModeValue('white', 'gray.700')}
			padding={{ base: 4, md: 6 }}
			rounded='xl'
			boxShadow='lg'
			flexDirection='column'
			gap='6'
		>
			<Flex
				alignItems={{ base: 'start', md: 'center' }}
				justifyContent='space-between'
				gap='4'
				flexDirection={{ base: 'column', md: 'row' }}
			>
				<Flex alignItems='center' gap='4' marginRight='2'>
					<Box
						bgColor='purple.200'
						width='4'
						height='8'
						rounded='4'
					/>
					<Heading as='h3' size='md'>
						{title}
					</Heading>
				</Flex>
				<Searchbar maxWidth={{ md: 'xs' }} />
				<Spacer display={{ base: 'none', md: 'unset' }} />
				<Flex
					gap='4'
					justifyContent='center'
					width={{ base: 'full', md: 'unset' }}
				>
					{onCreateRow && (
						<Button
							rightIcon={
								<Icon as={BiPlus} width='5' height='5' />
							}
							onClick={onCreateRow}
						>
							Create
						</Button>
					)}
					<IconButton
						icon={<Icon as={BiFilter} width='5' height='5' />}
						aria-label='Open filters menu.'
					/>
				</Flex>
			</Flex>
			<TableContainer>
				<ChakraTable colorScheme='blackAlpha'>
					{tableCaption && (
						<TableCaption>{tableCaption}</TableCaption>
					)}
					<Thead>
						<Tr>
							{headers.map(column => (
								<Th key={column.id}>
									{column.render('Header')}
								</Th>
							))}
							{hasActions && <Th>Actions</Th>}
						</Tr>
					</Thead>
					<Tbody>
						{rows.map(row => {
							prepareRow(row);
							return (
								<Tr key={row.id}>
									{row.cells.map((cell, index) => (
										<Td key={`${row.id}-${index}`}>
											{cell.render('Cell')}
										</Td>
									))}
									{hasActions && (
										<Td>
											<HStack>
												{onUpdateRow && (
													<IconButton
														colorScheme='gray'
														icon={
															<Icon
																as={BiEdit}
																width='5'
																height='5'
															/>
														}
														cursor='pointer'
														aria-label='Update entry'
														onClick={() =>
															onUpdateRow(
																row.original.id
															)
														}
													/>
												)}
												{onDeleteRow && (
													<IconButton
														colorScheme='red'
														icon={
															<Icon
																as={BiTrash}
																width='5'
																height='5'
															/>
														}
														cursor='pointer'
														aria-label='Delete entry'
														onClick={() =>
															onDeleteRow(
																row.original.id
															)
														}
													/>
												)}
											</HStack>
										</Td>
									)}
								</Tr>
							);
						})}
					</Tbody>
				</ChakraTable>
			</TableContainer>
		</Flex>
	);
};

interface Props {
	title: string;
	data: readonly any[];
	columns: readonly Column<any>[];
	onCreateRow?: () => void;
	onUpdateRow?: (id: string) => void;
	onDeleteRow?: (id: string) => void;
	tableCaption?: string;
}

export default Table;
