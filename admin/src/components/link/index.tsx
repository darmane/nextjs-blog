import NextLink, { LinkProps } from 'next/link';

const Link = ({ href, children, ...props }: Props) => {
	return (
		<NextLink href={href} passHref {...props}>
			{children}
		</NextLink>
	);
};

interface Props extends LinkProps {
	href: string;
	children: React.ReactNode;
}

export default Link;
