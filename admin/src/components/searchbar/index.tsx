import {
	InputGroup,
	Input,
	InputRightElement,
	Button,
	useColorModeValue,
	InputGroupProps,
} from '@chakra-ui/react';

const Searchbar = ({ ...props }: InputGroupProps) => {
	return (
		<InputGroup {...props}>
			<Input
				placeholder='Search in blog'
				bgColor={useColorModeValue('blackAlpha.50', 'gray.800')}
				rounded='lg'
			/>
			{/* <InputRightElement width='16'>
				<Button
					size='sm'
					bgColor={useColorModeValue('white', 'gray.700')}
					color={useColorModeValue('black', 'white')}
					boxShadow='sm'
					rounded='lg'
				>
					⌘ F
				</Button>
			</InputRightElement> */}
		</InputGroup>
	);
};

export default Searchbar;
