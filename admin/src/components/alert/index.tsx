import {
	Alert as ChakraAlert,
	AlertIcon,
	AlertTitle,
	AlertDescription,
	CloseButton,
	Spacer,
} from '@chakra-ui/react';
import { IAlert, useAlerts } from 'src/context/alerts';

const Alert = ({ id, status, title, description }: Props) => {
	const { removeAlert } = useAlerts();

	return (
		<ChakraAlert status={status}>
			<AlertIcon />
			<AlertTitle>{title}</AlertTitle>
			<AlertDescription>{description}</AlertDescription>
			{id && (
				<>
					<Spacer minWidth='4' />
					<CloseButton
						alignSelf=''
						position='relative'
						onClick={() => removeAlert(id)}
					/>
				</>
			)}
		</ChakraAlert>
	);
};

interface Props extends IAlert {}

export default Alert;
