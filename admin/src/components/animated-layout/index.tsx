import { Flex, FlexProps } from '@chakra-ui/react';
import { motion, Variants } from 'framer-motion';

const variants: Variants = {
	hidden: { opacity: 0 },
	enter: { opacity: 1 },
	exit: { opacity: 0 },
};

const MotionFlex = motion<FlexProps>(Flex);

const AnimatedLayout = ({ children, ...props }: Props) => {
	return (
		<MotionFlex
			as='main'
			initial='hidden'
			animate='enter'
			exit='exit'
			variants={variants}
			transition={{ duration: 0.3, type: 'linear' } as any}
			{...props}
		>
			{children}
		</MotionFlex>
	);
};

interface Props
	extends Omit<
		FlexProps,
		'onAnimationStart' | 'onDragStart' | 'onDragEnd' | 'onDrag'
	> {}

export default AnimatedLayout;
