import Navbar from '@/components/navbar';
import AnimatedLayout from '@/components/animated-layout';
import NavMenu from '@/components/nav-menu';
import AlertStack from '@/components/alert-stack';

const AdminLayout = (page: React.ReactElement) => {
	return (
		<>
			<AlertStack />
			<NavMenu />
			<Navbar />
			<AnimatedLayout
				marginLeft={{ base: 0, md: 80 }}
				paddingX={{ base: '4', md: '6' }}
				paddingY={{ base: '6', md: '8' }}
				gap={{ base: '4', md: '6' }}
				flexDirection='column'
			>
				{page}
			</AnimatedLayout>
		</>
	);
};

export default AdminLayout;
