import AlertStack from '@/components/shared/alert-stack';

const AuthLayout = (page: React.ReactElement) => {
	return (
		<>
			<AlertStack />
			{page}
		</>
	);
};

export default AuthLayout;
