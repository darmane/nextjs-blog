import { useState, useEffect } from 'react';

const useWindowSize = () => {
	const [windowSize, setWindowSize] = useState({
		width: 0,
		height: 0,
	});

	useEffect(() => {
		const handleResize = () => {
			if (!window) {
				return;
			}

			setWindowSize({
				width: window.innerWidth,
				height: window.innerHeight,
			});
		};

		window && window.addEventListener('resize', handleResize);

		// Call handler right away so state gets updated with initial window size
		handleResize();

		return () => {
			window && window.removeEventListener('resize', handleResize);
		};
	}, []);

	return windowSize;
};

export default useWindowSize;
