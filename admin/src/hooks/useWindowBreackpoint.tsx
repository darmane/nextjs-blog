import { useState, useEffect } from 'react';
import useWindowSize from './useWindowSize';

const breakpoints = {
	sm: 480,
	md: 768,
	lg: 992,
	xl: 1280,
	'2xl': 1536,
};

type WindowBreackpoint = 'sm' | 'md' | 'lg' | 'xl' | '2xl';

const useWindowBreakpoint = () => {
	const { width } = useWindowSize();

	const [breackpoint, setBreackpoint] = useState<WindowBreackpoint>('sm');

	useEffect(() => {
		const handleGetBreackpoint = () => {
			let newBreakpoint: WindowBreackpoint;
			if (width > breakpoints.sm) {
				newBreakpoint = 'md';
			} else if (width > breakpoints.md) {
				newBreakpoint = 'lg';
			} else if (width > breakpoints.lg) {
				newBreakpoint = 'xl';
			} else if (width > breakpoints.xl) {
				newBreakpoint = '2xl';
			} else {
				newBreakpoint = 'sm';
			}

			setBreackpoint(newBreakpoint);
		};

		handleGetBreackpoint();
	}, [width]);

	return breackpoint;
};

export default useWindowBreakpoint;
