import useSWR from 'swr';
import axios from 'axios';

const fetcher = (url: string) => axios.get(url).then(res => res.data);

const useAxiosSWR = (key: string) => {
	return useSWR(key, fetcher);
};

export default useAxiosSWR;
