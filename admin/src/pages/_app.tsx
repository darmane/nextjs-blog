import { NextPage } from 'next';
import type { AppProps } from 'next/app';
import { ChakraProvider } from '@chakra-ui/react';
import theme from '@/lib/theme';
import AppContextProvider from '@/context/index';

type NextPageWithLayout = NextPage & {
	getLayout?: (page: React.ReactElement) => React.ReactNode;
};

type AppPropsWithLayout = AppProps & {
	Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
	const getLayout = Component.getLayout ?? (page => page);

	return (
		<ChakraProvider theme={theme}>
			<AppContextProvider>
				{getLayout(<Component {...pageProps} />)}
			</AppContextProvider>
		</ChakraProvider>
	);
}

export default MyApp;
