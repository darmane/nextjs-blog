import slugify from 'slugify';

const getSlug = (value: string) => slugify(value, { lower: true, trim: true });

export default getSlug;
