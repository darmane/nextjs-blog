import axios, { AxiosRequestHeaders } from 'axios';

const API_URL = 'http://localhost:8000';

const apiFetcher = async (
	url: string,
	method: 'GET' | 'POST' | 'PUT' | 'DELETE' = 'GET',
	payload?: any,
	headers?: AxiosRequestHeaders
): Promise<{ data?: any | any[]; error?: any }> => {
	try {
		const res = await axios.request({
			data: payload,
			method,
			url: `${API_URL}${url}`,
			headers: headers,
		});

		return { data: res.data };
	} catch (error) {
		return { error };
	}
};

export default apiFetcher;
