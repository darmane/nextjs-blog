import { extendTheme, ThemeConfig } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools';

const styles = {
	global: (props: any) => ({
		body: {
			bg: mode('blackAlpha.50', 'blackAlpha.600')(props),
		},
	}),
};

const config: ThemeConfig = {
	initialColorMode: 'light',
	// useSystemColorMode: true,
};

const theme = extendTheme({ styles, config });

export default theme;
